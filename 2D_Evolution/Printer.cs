﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using _2D_Evolution.Entities;

namespace _2D_Evolution
{
    class Printer
    {
        // Prevents printing to speed up evolution
        public static bool NoPrint = false;

        // Usually only called once
        public static void PrintMap(List<Node> nodeList)
        {
            if(NoPrint) return;
            foreach (Node node in nodeList)
            {
                Console.SetCursorPosition(node.X, node.Y);
                Console.Write(node.Character);
            }
        }

        // Used for printing aStar paths
        public static void PrintPath(List<Node> nodeList, ConsoleColor pathColor, char pathChar)
        {
            if (NoPrint) return;
            foreach (Node node in nodeList)
            {
                Console.SetCursorPosition(node.X, node.Y);
                Console.ForegroundColor = pathColor;
                Console.Write(pathChar);
                Console.ResetColor();
            }
        }

        // Prints the creature's current position and updates the map
        public static void PrintCreature(Creature c)
        {
            if (NoPrint) return;
            // Current Position
            Console.SetCursorPosition(c.Node.X, c.Node.Y);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write('☺');
            Console.ResetColor();

            // Last Position
            if (c.PreviousNode == null) return;
            Console.SetCursorPosition(c.PreviousNode.X, c.PreviousNode.Y);
            Console.Write(c.PreviousNode.Character);
        }

        // Removes the creature's image from the map
        public static void ClearCreature(Creature c)
        {
            if (NoPrint) return;
            Console.SetCursorPosition(c.Node.X, c.Node.Y);
            Console.Write(c.Node.Character);
        }

        public static void Sleep()
        {
            if (NoPrint) return;
            Thread.Sleep(10);
        }
    }
}
