﻿using System;

namespace _2D_Evolution.Entities
{
    public static class Gene
    {
        private static readonly Random _random = new Random();

        public const int UU = 8;
        public const int DD = 2;
        public const int LL = 4;
        public const int RR = 6;
        public const int UL = 7;
        public const int UR = 9;
        public const int DL = 1;
        public const int DR = 3;

        public static int GetRandomGene()
        {
            int gene = _random.Next(0, 7);
            switch (gene)
            {
                case 0:
                    return UU;
                case 1:
                    return DD;
                case 2:
                    return LL;
                case 3:
                    return RR;
                case 4:
                    return UL;
                case 5:
                    return UR;
                case 6:
                    return DL;
                case 7:
                    return DR;
                default:
                    throw new Exception("Attempted to retrieve malformed gene"); //Error
            }
        }
    }
}
