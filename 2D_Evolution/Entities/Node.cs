﻿namespace _2D_Evolution.Entities
{
    class Node
    {
        public int X;
        public int Y;
        public bool Start;
        public bool End;
        public bool Passable;
        public char Character;
        public Node Parent;
    }
}
