﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2D_Evolution.Entities
{
    class Creature
    {
        public DNA DNA = new DNA();
        public Node Node;
        public Node PreviousNode;
        public int StepCount = 0;
        public int EvolutionScore;
    }
}
