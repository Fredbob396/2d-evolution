﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _2D_Evolution.Entities
{
    class NodeList
    {
        public List<Node> List = new List<Node>();

        public NodeList(string file)
        {
            Load(file);
        }

        private void Load(string file)
        {
            for (int y = 0; y < File.ReadAllLines(file).Length; y++)
            {
                string line = File.ReadAllLines(file)[y];
                for (int x = 0; x < line.Length; x++)
                {
                    char c = line[x];
                    if (c.Equals('X'))
                    {
                        List.Add(new Node
                        {
                            X = x,
                            Y = y,
                            Passable = false,
                            Start = false,
                            End = false,
                            Character = c
                        });
                    }
                    else if (c.Equals('S'))
                    {
                        List.Add(new Node
                        {
                            X = x,
                            Y = y,
                            Passable = true,
                            Start = true,
                            End = false,
                            Character = c
                        });
                    }
                    else if (c.Equals('E'))
                    {
                        List.Add(new Node
                        {
                            X = x,
                            Y = y,
                            Passable = true,
                            Start = false,
                            End = true,
                            Character = c
                        });
                    }
                    else if (c.Equals(' '))
                    {
                        List.Add(new Node
                        {
                            X = x,
                            Y = y,
                            Passable = true,
                            Start = false,
                            End = false,
                            Character = c
                        });
                    }
                    else
                    {
                        throw new Exception($"Invalid Map Character: '{c}'");
                    }
                }
            }
        }

        public Node GetNode(int x, int y)
        {
            return List.FirstOrDefault(n => n.X == x && n.Y == y);
        }

        public Node GetStartNode()
        {
            return List.FirstOrDefault(x => x.Start);
        }

        public Node GetEndNode()
        {
            return List.FirstOrDefault(x => x.End);
        }

        public List<Node> GetNeighbors(Node node)
        {
            List<Node> neighbors = new List<Node>();
            for (int xx = -1; xx <= 1; xx++)
            {
                for (int yy = -1; yy <= 1; yy++)
                {
                    if (xx == 0 && yy == 0)
                    {
                        continue;
                    }
                    Node neighbor = List.FirstOrDefault(n => 
                        n.X == node.X + xx && 
                        n.Y == node.Y + yy);
                    if (neighbor != null && neighbor.Passable)
                    {
                        neighbors.Add(neighbor);
                    }
                }
            }
            return neighbors;
        }
    }
}
