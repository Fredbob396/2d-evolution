﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2D_Evolution.Entities
{
    class DNA
    {
        public List<int> Genome = new List<int>();
        private const int DNASegmentLength = 10;
        private const int MinChunksToMutate = 1;
        private const int MaxChunksToMutate = 3;
        private const int MinMutationRate = 25;
        private const int MaxMutationRate = 50;
        private readonly Random _random = new Random();

        public void BuildRandomDNA()
        {
            BuildGenomeFromString(BuildRandomGenomeString(DNASegmentLength));
        }

        public void BuildGenomeFromString(string genomeString)
        {
            Genome = new List<int>();
            foreach (char c in genomeString)
            {
                Genome.Add(int.Parse(c.ToString()));
            }
        }

        public string BuildRandomGenomeString(int length)
        {
            string genomeString = "";
            for (int i = 0; i < Math.Pow(length, 2); i++)
            {
                genomeString += Gene.GetRandomGene().ToString();
            }
            return genomeString;
        }

        public string GetGenomeString()
        {
            return string.Join("", Genome);
        }

        public void MutateDNA()
        {
            List<List<int>> genomeChunks = GetGenomeChunks();

            int numChunksToMutate = _random.Next(MinChunksToMutate, MaxChunksToMutate + 1);
            List<int> chunksToMutate = new List<int>();

            // Determines which Genome chunks to mutate
            for (int i = 0; i < numChunksToMutate; i++)
            {
                int chunkToMutate = _random.Next(0, genomeChunks.Count);
                if (!chunksToMutate.Contains(chunkToMutate))
                {
                    chunksToMutate.Add(chunkToMutate);
                }
                else
                {
                    while (chunksToMutate.Contains(chunkToMutate))
                    {
                        chunkToMutate = _random.Next(0, genomeChunks.Count);
                    }
                    chunksToMutate.Add(chunkToMutate);
                }
                if (chunkToMutate == 0)
                {
                    int brk = 0;
                }
            }

            // Mutate each chunk
            foreach (int i in chunksToMutate)
            {
                List<int> chunk = genomeChunks[i];
                int mutationRate = _random.Next(MinMutationRate, MaxMutationRate + 1);
                for (int j = 0; j < chunk.Count; j++)
                {
                    if (_random.Next(0, 100) <= mutationRate)
                    {
                        chunk[j] = Gene.GetRandomGene();
                    }
                }
            }
            BuildGenomeFromString(CondenseChunks(genomeChunks));
        }

        // https://stackoverflow.com/a/30248074
        public List<List<int>> GetGenomeChunks()
        {
            List<List<int>> list = new List<List<int>>();
            for (int i = 0; i < Genome.Count; i += DNASegmentLength)
                list.Add(Genome.GetRange(i, Math.Min(DNASegmentLength, Genome.Count - i)));
            return list;
        }

        // Condenses genome chunks back into a DNA string
        public string CondenseChunks(List<List<int>> genomeChunks)
        {
            // Acts like two nested foreach loops adding all the nest list components to a string
            return genomeChunks.SelectMany(chunk => chunk)
                .Aggregate("", (current, i) => current + i);
        }
    }
}
