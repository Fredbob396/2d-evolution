﻿using System;
using System.Collections.Generic;
using _2D_Evolution.Entities;

namespace _2D_Evolution
{
    class AStar
    {
        private readonly NodeList _nodeList;
        public AStar(NodeList nodeList)
        {
            _nodeList = nodeList;
        }

        // Gets a list of nodes that make up the best path
        public List<Node> GetPath(Node startNode, Node endNode)
        {
            List<Node> closedSet = new List<Node>();
            List<Node> openSet = new List<Node>
            {
                startNode
            };

            while (openSet.Count != 0)
            {
                Node current = GetNodeWithLowestFScore(openSet, endNode);
                if (current == endNode)
                {
                    // Gets and returns the most efficient path
                    List<Node> aStar = new List<Node>();
                    while (current.Parent != null)
                    {
                        aStar.Add(current);
                        current = current.Parent;
                    }
                    ClearParents();
                    return aStar;
                }

                openSet.Remove(current);
                closedSet.Add(current);

                foreach (Node neighbor in _nodeList.GetNeighbors(current))
                {
                    if (closedSet.Contains(neighbor) || openSet.Contains(neighbor)) continue;
                    neighbor.Parent = current;
                    openSet.Add(neighbor);
                }
            }
            ClearParents();
            return null;
        }

        // Cost of getting from the current node to end node
        private int FScore(Node node, Node endNode)
        {
            int dx = Math.Abs(node.X - endNode.X);
            int dy = Math.Abs(node.Y - endNode.Y);

            return dx + dy;
        }

        // Gets the lowest fScore of the entire node list
        private Node GetNodeWithLowestFScore(List<Node> nodeSet, Node endNode)
        {
            Node lowestFNode = null;
            int lowestF = Int32.MaxValue;
            foreach (Node node in nodeSet)
            {
                int f = FScore(node, endNode);
                if (f < lowestF)
                {
                    lowestF = f;
                    lowestFNode = node;
                }
            }
            return lowestFNode;
        }

        // Clears set parents for next aStar calculation
        private void ClearParents()
        {
            foreach (Node node in _nodeList.List)
            {
                node.Parent = null;
            }
        }
    }
}