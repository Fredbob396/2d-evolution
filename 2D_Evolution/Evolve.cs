﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using _2D_Evolution.Entities;

namespace _2D_Evolution
{
    class Evolve
    {
        private readonly NodeList _nodeList = new NodeList("level.txt");
        private List<Creature> _creatures = new List<Creature>();
        private Random _random = new Random();
        private readonly AStar _aStar;
        private const int CreaturesPerGeneration = 25;
        private const int MaxAncestors = 5;

        private int previousBest = int.MaxValue;
        private int currentBest = int.MaxValue;

        public Evolve()
        {
            _aStar = new AStar(_nodeList);
            InitializeCreatures();
            EvolutionLoop();
        }

        private void InitializeCreatures()
        {
            Printer.NoPrint = true;
            Printer.PrintMap(_nodeList.List);
            for (int i = 0; i < CreaturesPerGeneration; i++)
            {
                _creatures.Add(new Creature());
            }
            foreach (Creature c in _creatures)
            {
                c.DNA.BuildRandomDNA();
                c.Node = _nodeList.GetStartNode();
            }
        }

        // Main evolution loop
        private void EvolutionLoop()
        {
            long generation = 1;
            while (true)
            {
                // Print new best score if it's better than the previous
                if (currentBest < previousBest)
                {
                    previousBest = currentBest;
                    Debug.Print($"Generation: {generation}");
                    Debug.Print($"BEST SCORE: {currentBest}");
                }
                DetermineCreatureScores();
                ReduceToBestCreatures();
                BuildNextGeneration();
                generation++;
            }
        }

        // Creates the next generation of creatures
        private void BuildNextGeneration()
        {
            List<Creature> newCreatures = new List<Creature>();
            foreach (Creature c in _creatures)
            {
                //Debug.Print($"{c.EvolutionScore}: {c.DNA.GetGenomeString()}");
                for (int i = 0; i < CreaturesPerGeneration; i++)
                {
                    // "Clones" the creature
                    var newC = new Creature();
                    newC.DNA.BuildGenomeFromString(c.DNA.GetGenomeString());
                    newC.Node = _nodeList.GetStartNode();
                    newCreatures.Add(newC);

                    // Preserves one "pure" creature
                    if (i == 0) continue;

                    newC.DNA.MutateDNA();
                }
            }
            _creatures.Clear();
            _creatures.AddRange(newCreatures);
        }

        // Determines the evolution score of every creature in the list
        private void DetermineCreatureScores()
        {
            foreach (Creature c in _creatures)
            {
                foreach (int gene in c.DNA.Genome)
                {
                    TryMove(c, gene);
                    Printer.PrintCreature(c);
                    // Creature has reached goal!
                    if (c.Node.Equals(_nodeList.GetEndNode())) break;
                    Printer.Sleep();
                }
                c.EvolutionScore = GetEvolutionScore(c);
                c.Node = _nodeList.GetStartNode();
                c.PreviousNode = null;
                Printer.ClearCreature(c);
            }
        }

        // Reduces list to all creatures with the best evolution score
        private void ReduceToBestCreatures()
        {
            _creatures = _creatures.OrderBy(x => x.EvolutionScore).ToList();
            int bestScore = _creatures[0].EvolutionScore;

            currentBest = bestScore; // for printing

            _creatures = _creatures.Where(x => x.EvolutionScore == bestScore).ToList();
            if (_creatures.Count >= MaxAncestors)
            {
                // Shuffle list and purge anything over the Max Ancestors value
                _creatures = _creatures.OrderBy(x => _random.Next()).ToList();
                _creatures.RemoveRange(MaxAncestors, _creatures.Count - MaxAncestors);
            }
        }

        // Attempts to move the creature in the direction that the gene dictates
        private void TryMove(Creature c, int gene)
        {
            Node targetNode = GetTargetNode(c, gene);
            if (targetNode != null && targetNode.Passable)
            {
                c.PreviousNode = c.Node;
                c.Node = targetNode;
            }
            c.StepCount++;
        }

        // Retrieves the node in the direction that the gene dictates
        private Node GetTargetNode(Creature c, int gene)
        {
            switch (gene)
            {
                case Gene.UU:
                    return _nodeList.GetNode(c.Node.X, c.Node.Y - 1);
                case Gene.DD:
                    return _nodeList.GetNode(c.Node.X, c.Node.Y + 1);
                case Gene.LL:
                    return _nodeList.GetNode(c.Node.X - 1, c.Node.Y);
                case Gene.RR:
                    return _nodeList.GetNode(c.Node.X + 1, c.Node.Y);
                case Gene.UL:
                    return _nodeList.GetNode(c.Node.X - 1, c.Node.Y - 1);
                case Gene.UR:
                    return _nodeList.GetNode(c.Node.X + 1, c.Node.Y - 1);
                case Gene.DL:
                    return _nodeList.GetNode(c.Node.X - 1, c.Node.Y + 1);
                case Gene.DR:
                    return _nodeList.GetNode(c.Node.X + 1, c.Node.Y + 1);
                default:
                    return null;
            }
        }

        /* Calculates the "Evolution Score" of the current creature
         * The lower the score, the better the genome
         * Forumula: EvSc = (S)*(1+D)
         * Key:
         *  EvSc = Evolution Score
         *  S    = Steps Taken
         *  D    = Distance from Goal
         */
        private int GetEvolutionScore(Creature c)
        {
            int goalDistance = _aStar.GetPath(c.Node, _nodeList.GetEndNode()).Count;
            return (c.StepCount) * (1 + goalDistance);
        }

        // Draws a simple path from the start node to the end node
        private void TestAStar()
        {
            Printer.PrintMap(_nodeList.List);
            var aStarPath = new AStar(_nodeList).GetPath(_nodeList.GetStartNode(), _nodeList.GetEndNode());
            Printer.PrintPath(aStarPath, ConsoleColor.Yellow, '#');
        }
    }
}
